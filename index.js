// 'use strict'

// Теоретический вопрос
//
// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//
// DOM это дерево логичесской структуры документа
//
// Задание
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).
//
// Технические требования:
//
// Создать функцию, которая будет принимать на вход массив и
// опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список
// (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования контента списка
// перед выведением его на страницу;




const userData = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const domElem = prompt('enter DOM element', ['body']);


function returnList (userData, domElem) {
    const newUl = document.createElement('ul');
    if (domElem === 'body') {
        document.querySelector('body').append(newUl)
    } else {
        const optinalEl = document.createElement(`${domElem}`);
        optinalEl.append (newUl);
        document.querySelector('body').append(optinalEl);
    }
        return userData.map((element) => {
        const newLi = document.createElement('li');
        newUl.append(newLi);
        return (newLi.innerHTML = element);
    })
}

returnList (userData, domElem);
